import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder
from sklearn.feature_selection import VarianceThreshold

from sklearn.cluster import AffinityPropagation

from sklearn.tree import ExtraTreeClassifier, DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression

from sklearn.preprocessing import Normalizer

import math

file_name = "secom_npy_all.npy"
data = np.load(file_name)

data = pd.DataFrame(data)
print(data.shape)
data.dropna(inplace=True)

row, column = data.shape

X = data.iloc[:, 0:column - 1]
y = data.iloc[:, column - 1]

y = LabelEncoder().fit_transform(y)

X = np.array(X)

all_zero_column_remover = VarianceThreshold(threshold=0)

X = all_zero_column_remover.fit_transform(X)

_, column = data.shape
normalizer = Normalizer()

Cov_X_two = np.corrcoef(X, rowvar=False)

clusterer = AffinityPropagation()
clusterer.fit(Cov_X_two)

number_of_clusters = len(clusterer.cluster_centers_indices_)

features_under_each_cluster = {i: np.where(clusterer.labels_ == i)[0] for i in range(number_of_clusters)}

selected_features = []
fraction_of_features_to_select = 0.30

for i in range(number_of_clusters):
    features_under_current_cluster = features_under_each_cluster[i]
    number_of_features_to_select = math.ceil(len(features_under_current_cluster) * fraction_of_features_to_select)
    selected_features.append(np.random.choice(features_under_current_cluster, size=number_of_features_to_select))
final_arr = []
# print(selected_features)
for i in range(len(selected_features)):
    final_arr = np.append(final_arr, selected_features[i])
# print(np.asarray(selected_features))
final_arr = final_arr.astype(int)
data = data.values
a = np.array(data)
print(a.shape)
# f=np.take(a[:],final_arr)
# c=pd.DataFrame(f,columns=data[:,:])
f = a[:, final_arr]
# print(f)
print(f.shape)
######################################
# np.savetxt("weig.csv", f, delimiter=",")
df = pd.read_csv('dset/secom_label.csv',  header=None, na_values='na')
# D = D.astype(np.float)
# print(df.shape)
# D = pd.DataFrame(D)
# p=df.values
l = df.iloc[:, -1]
# k=l.drop_duplicates()
# print()
# print(l)
# print(l.shape)
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
# print("Shape")
x=np.concatenate((f,df),axis = 1)
# print(df.head)
# print(l.head)
# x.to_csv("secom_weka.csv")
# df = pd.DataFrame(x)
# df.to_csv("secom_weka.csv")
################# Correlation accu: ############

X_train, X_test, y_train, y_test = train_test_split(f, l, test_size=0.2)
# dtc = DecisionTreeClassifier(random_state=0)
# dtc.fit(X_train, y_train)
# 4 features in the iris dataset
# y_pred_class = dtc.predict(X_test)
# from sklearn import metrics
# print("Correlation accu:")
# print(metrics.accuracy_score(y_test, y_pred_class))

############ full data Accuracy ##########

#
# X_train, X_test, y_train, y_test = train_test_split(data, l, test_size=0.2)
# dtc = DecisionTreeClassifier(random_state=0)
#
# # 2. Fit
# dtc.fit(X_train, y_train)
#
# # 3. Predict, there're 4 features in the iris dataset
# y_pred_class = dtc.predict(X_test)
from sklearn import metrics
#
# print("Full accu")
# print(metrics.accuracy_score(y_test, y_pred_class))

# ############ ID3  ###############
# X_train, X_test, y_train, y_test = train_test_split(f, l, test_size=0.2)
# dtc = DecisionTreeClassifier(criterion = "gini", random_state = 0,
#                                max_depth=3, min_samples_leaf=5)
# dtc.fit(X_train, y_train)
# y_pred_class = dtc.predict(X_test)
# print("Accuracy of gini ID3 Correaltaion")
# print (metrics.accuracy_score(y_test,y_pred_class)*100)

########## ID3 full ##########

# X_train, X_test, y_train, y_test = train_test_split(data, l, test_size=0.2)
#
# dtc = DecisionTreeClassifier(criterion = "gini", random_state = 0,
#                                max_depth=3, min_samples_leaf=5)
# dtc.fit(X_train, y_train)
# y_pred_class = dtc.predict(X_test)
# print("Accuracy of gini ID3 Full Data")
# print (metrics.accuracy_score(y_test,y_pred_class)*100)

############## BAGGING ############

# from sklearn import model_selection
# from sklearn.ensemble import BaggingClassifier
# from sklearn.tree import DecisionTreeClassifier
# X = f
# Y = l
# seed = 7
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# cart = DecisionTreeClassifier()
# num_trees = 10
# model = BaggingClassifier(base_estimator=cart, n_estimators=num_trees, random_state=seed)
# results = model_selection.cross_val_score(model, X, Y, cv=kfold)
# print(results.mean())

############## BAGGING full ############
# from sklearn import model_selection
# from sklearn.ensemble import BaggingClassifier
# from sklearn.tree import DecisionTreeClassifier
# X = data
# Y = l
# seed = 7
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# cart = DecisionTreeClassifier()
# num_trees = 10
# model = BaggingClassifier(base_estimator=cart, n_estimators=num_trees, random_state=seed)
# results = model_selection.cross_val_score(model, X, Y, cv=kfold)
# print(results.mean())

############ Random Forest ############
# from sklearn import model_selection
# from sklearn.ensemble import RandomForestClassifier
X = data
Y = l
# seed = 7
# num_trees = 10
# max_features = 3
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# model = RandomForestClassifier(n_estimators=num_trees, max_features=max_features)
# results = model_selection.cross_val_score(model, X, Y, cv=kfold)
# print(results.mean())
#############AdaBoost Algorithms##########
# from sklearn import model_selection
# from sklearn.ensemble import AdaBoostClassifier
# seed = 7
# num_trees =10
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# model = AdaBoostClassifier(n_estimators=num_trees, random_state=seed)
# results = model_selection.cross_val_score(model, X, Y, cv=kfold)
# print(results.mean())

##############Voting Ensemble####################
# from sklearn import model_selection
# from sklearn.linear_model import LogisticRegression
# from sklearn.tree import DecisionTreeClassifier
# from sklearn.svm import SVC
# from sklearn.ensemble import VotingClassifier
# seed = 7
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# # create the sub models
# estimators = []
# # model1 = LogisticRegression()
# # estimators.append(('logistic', model1))
# # model2 = DecisionTreeClassifier()
# # estimators.append(('cart', model2))
# model3 = SVC()
# estimators.append(('svm', model3))
# # create the ensemble model
# # ensemble = VotingClassifier(estimators)
# results = model_selection.cross_val_score(model3, X, Y, cv=kfold)
# print(results.mean())

###############GNB#############
