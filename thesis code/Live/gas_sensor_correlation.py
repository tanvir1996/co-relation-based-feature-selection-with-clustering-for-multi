import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import train_test_split
from sklearn.cluster import AffinityPropagation

from sklearn.tree import ExtraTreeClassifier, DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression

from sklearn.preprocessing import Normalizer

import math

file_name = "gasfile.npy"
data = np.load(file_name)

data = pd.DataFrame(data)
print( data.shape)
label= data.iloc[:,0]
# print( data.shape)

data= data.iloc[:,1:]
# print( data)
row, column = data.shape

X = data.iloc[:, 0:column - 1]
y = data.iloc[:, column - 1]

y = LabelEncoder().fit_transform(y)

X = np.array(X)

all_zero_column_remover = VarianceThreshold(threshold=0)

X = all_zero_column_remover.fit_transform(X)

_, column = data.shape
normalizer = Normalizer()

Cov_X_two = np.corrcoef(X, rowvar=False)

clusterer = AffinityPropagation()
clusterer.fit(Cov_X_two)

number_of_clusters = len(clusterer.cluster_centers_indices_)

features_under_each_cluster = {i: np.where(clusterer.labels_ == i)[0] for i in range(number_of_clusters)}

selected_features = []
fraction_of_features_to_select = 0.30

for i in range(number_of_clusters):
    features_under_current_cluster = features_under_each_cluster[i]
    number_of_features_to_select = math.ceil(len(features_under_current_cluster) * fraction_of_features_to_select)
    selected_features.append(np.random.choice(features_under_current_cluster, size=number_of_features_to_select))
final_arr = []
# print(selected_features)
for i in range(len(selected_features)):
    final_arr = np.append(final_arr, selected_features[i])
# print(np.asarray(selected_features))
final_arr = final_arr.astype(int)
data = data.values
a = np.array(data)
# print(a.shape)
# f=np.take(a[:],final_arr)
# c=pd.DataFrame(f,columns=data[:,:])
f = a[:, final_arr]
# print(f)
print(f.shape)
############ Accuracy ##########

# X_train, X_test, y_train, y_test = train_test_split(f, label, test_size=0.2)
# # print(X_train.shape, y_train.shape)
# # print(X_test.shape, y_test.shape)
# dtc = DecisionTreeClassifier(random_state=0)
#
# # 2. Fit
# dtc.fit(X_train, y_train)
#
# # 3. Predict, there're 4 features in the iris dataset
# y_pred_class = dtc.predict(X_test)
# from sklearn import metrics
# print("Cor Accuracy")
# print(metrics.accuracy_score(y_test, y_pred_class))
############ full data Accuracy ##########


# X_train, X_test, y_train, y_test = train_test_split(data, label, test_size=0.2)
# # print(X_train.shape, y_train.shape)
# # print(X_test.shape, y_test.shape)
# dtc = DecisionTreeClassifier(random_state=0)
#
# # 2. Fit
# dtc.fit(X_train, y_train)
#
# # 3. Predict, there're 4 features in the iris dataset
# y_pred_class = dtc.predict(X_test)
# from sklearn import metrics
# print("Full accu")
# print(metrics.accuracy_score(y_test, y_pred_class))
##################
X= f
Y= label
# from sklearn import model_selection
# from sklearn.linear_model import LogisticRegression
# from sklearn.tree import DecisionTreeClassifier
# from sklearn.svm import SVC
# from sklearn.ensemble import VotingClassifier
# seed = 7
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# # create the sub models
# estimators = []
# model1 = LogisticRegression()
# estimators.append(('logistic', model1))
# model2 = DecisionTreeClassifier()
# estimators.append(('cart', model2))
# model3 = SVC()
# estimators.append(('svm', model3))
# # create the ensemble model
# ensemble = VotingClassifier(estimators)
# # results = model_selection.cross_val_score(ensemble, X, Y, cv=kfold)
# # print(results.mean())
# results1 = model_selection.cross_val_score(model1, X, Y, cv=kfold)
# results2 = model_selection.cross_val_score(model2, X, Y, cv=kfold)
# results3 = model_selection.cross_val_score(model3, X, Y, cv=kfold)
# print("Logistic")
# print(results1.mean())
# print("Cart")
# print(results2.mean())
# print("Svm")
# print(results3.mean())
#################################
from sklearn import model_selection
from sklearn.ensemble import RandomForestClassifier
# X = f
Y = label
# seed = 7
# num_trees = 10
# max_features = 3
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# model = RandomForestClassifier(n_estimators=num_trees, max_features=max_features)
# results = model_selection.cross_val_score(model, X, Y, cv=kfold)
# print(results.mean())
#
# X=data
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# model = RandomForestClassifier(n_estimators=num_trees, max_features=max_features)
# results = model_selection.cross_val_score(model, X, Y, cv=kfold)
# print(results.mean())
#############AdaBoost Algorithms##########
X=f
# from sklearn import model_selection
# from sklearn.ensemble import AdaBoostClassifier
# seed = 7
# num_trees =10
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# model = AdaBoostClassifier(n_estimators=num_trees, random_state=seed)
# results = model_selection.cross_val_score(model, X, Y, cv=kfold)
# print(results.mean())
##########BAGGING##########
# from sklearn import model_selection
# from sklearn.ensemble import BaggingClassifier
# from sklearn.tree import DecisionTreeClassifier
# X = f
#
# seed = 7
# kfold = model_selection.KFold(n_splits=10, random_state=seed)
# cart = DecisionTreeClassifier()
# num_trees = 10
# model = BaggingClassifier(base_estimator=cart, n_estimators=num_trees, random_state=seed)
# results = model_selection.cross_val_score(model, X, Y, cv=kfold)
# print(results.mean())
###########Votting
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import VotingClassifier
seed = 7
kfold = model_selection.KFold(n_splits=10, random_state=seed)
# create the sub models
estimators = []
model1 = LogisticRegression()
estimators.append(('logistic', model1))
model2 = DecisionTreeClassifier()
estimators.append(('cart', model2))
model3 = SVC()
estimators.append(('svm', model3))
# create the ensemble model
ensemble = VotingClassifier(estimators)
results = model_selection.cross_val_score(model3, X, Y, cv=kfold)
print(results.mean())