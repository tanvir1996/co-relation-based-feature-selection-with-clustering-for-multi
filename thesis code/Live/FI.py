import pandas as pd
df = pd.read_csv('dset/aps_failure_training_set.csv', skiprows=21, header=None, na_values='na')
print(df.shape)
l = df.iloc[:,0].values
X = df.iloc[0:60000,1:]
print(X.shape)
print(l.shape)
from fancyimpute import KNN

X_filled_knn = KNN(k=3).complete(X)

print(X_filled_knn)
print(X)
print(fancyimpute.__version__)
import numpy as np
np.save('output_training.npy',X_filled_knn)
print(np.load('output_training.npy'))